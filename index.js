// Dependency packages
const cheerio = require('cheerio');
const axios = require('axios');
const async = require('async');
const MongoClient = require('mongodb').MongoClient;

// basic variables
const mongoURL = 'mongodb://localhost:27017/scrapy';
const dbName = 'scrapy';
const collectionName = 'articles';
const categoryCount = 1;
const articlesCount = 1;

const getSiteContent = (url, callback) => {
    /**
     * This is a general helper function
     * call the given url and return all
     * it's content
     */
    axios.get(url)
    .then(response => {
        callback(null, response.data);
    })
    .catch(error => {
        // console.log(error);
        callback(null);
    });
}

const scrapeMeta = (url, callback) => {
    /**
     * This is a helperfunction, this will scrape the
     * Open Graph meta tags from the given url
     */
    getSiteContent(url, (err, result) => {
        if (result) {
            const pageContent = cheerio.load(result);
            let title = pageContent("meta[property='og:title']").attr("content");
            let image = pageContent("meta[property='og:image']").attr("content");
            let description = pageContent("meta[property='og:description']").attr("content");
            if (title || image || description) {
                callback(null, { title, image, description });
            } else {
                callback(null);
            }
        } else {
            callback(null)
        }
    });
}

const scrapeHeader = (siteName, siteLink, $, callback) => {
    /**
     * Since web page can have n number of articles
     * for demo purpose we are only scraping the first page(ie, business) and
     * 10 articles in that page.
     */
    console.log('Working on', siteName);
    const siteTitle = $('title').text();
    let categories;
    // This is where we are deciding which html elements we need to scrape
    // the elements differ from each other because each site is unique in UI.
    if (siteName == 'timeofindia') {
        categories = $('div[class="DbwT8"]').find('nav > ul > li > a');
    } else if (siteName == 'firstpost') {
        categories = $('div[class="menu-wrap container"]').find('ul > li > a');
    } else if (siteName == 'indianexpress') {
        categories = $('div[class="mainnav"]').find('ul > li > a');
    } else if (siteName == 'moneycontrol') {
        categories = $('ul[class="market_listmenu"]').find('li > a');
    }
    let allMeta = [];
    async.each(categories.toArray().slice(0, categoryCount), (category, nextCategory) => {
        let pageURL = category.attribs.href;
        if (siteName == 'indianexpress') {
            pageURL = siteLink+pageURL;
        }
        getSiteContent(category.attribs.href, (err, result) => {
            if (result) {
                let hrefLinks = [];
                const pageContent = cheerio.load(result);
                let allLinks = pageContent('a');
                async.each(allLinks.toArray().slice(0, articlesCount), (sourceLink, nextLink) => {
                    let link = sourceLink.attribs.href;
                    if (link && hrefLinks.indexOf(link) == -1 && link.startsWith("http")) {
                        hrefLinks.push(link);
                        scrapeMeta(link, (err, result) => {
                            if (result) {
                                result.link = link;
                                result.site_name = siteName;
                                result.site_title = siteTitle;
                                allMeta.push(result);
                                // console.log(result);
                            }
                            nextLink();
                        })
                    } else {
                        nextLink();
                    }
                }, err => {
                    if (err) {
                        console.log(err);
                    } else {
                        nextCategory();
                    }
                })
            } else {
                // console.log('Error at getting site content', err);
                nextCategory();
            }
        })
    }, err => {
        if (err) {
            console.log(err);
            callback(null);
        } else {
            if (allMeta.length > 0) {
                // I am initializing mongo connection everytime for a new category
                // I can make it as utils as a single, but for this script it's more than
                // enough for the demo.
                MongoClient.connect(mongoURL, {useUnifiedTopology: true}, function(err, db) {
                    if (err) throw err;
                    var dbo = db.db(dbName);
                    // var myobj = { name: "Company Inc", address: "Highway 37" };
                    dbo.collection(collectionName).insertMany(allMeta, function(err, res) {
                      if (err) throw err;
                      console.log("Document Inserted For", siteName);
                      db.close();
                      callback(null, siteName);
                    });
                });
            } else {
                console.log('No Data available for '+siteName+' so skipping');
                callback(null, siteName);
            }
        }
    })
}

/**
 * It's all strarts here
 */
async.waterfall([
    (cb) => {
        const siteDetails = {
            timesofindia: 'https://timesofindia.indiatimes.com/',
            firstpost: 'https://www.firstpost.com/',
            indianexpress: 'https://indianexpress.com',
            moneycontrol: 'https://www.moneycontrol.com/'
        }
        cb(null, siteDetails);
    }, (siteDetails, cb) => {
        async.eachSeries(Object.keys(siteDetails), (site, nextSite) => {
            let siteLink = siteDetails[site];
            getSiteContent(siteLink, (err, result) => {
                if (siteLink && result) {
                    const siteContent = cheerio.load(result);
                    scrapeHeader(site, siteLink, siteContent, (err, result) => {
                        if (result) {
                            console.log('Done with', result);
                            console.log('*********');
                            nextSite();
                        } else {
                            console.log(err);
                            cb(null);
                        }
                    })
                } else {
                    nextSite()
                }
            })
        }, err => {
            if (err) {
                console.log(err);
                cb(null);
            } else {
                console.log(`All Done, check mongo DB -> ${dbName}, Collection -> ${collectionName}`);
            }
        })
    }
])