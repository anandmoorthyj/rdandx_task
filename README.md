# RD&X Task
## Make sure the followings are installed before running the script

1. MongoDB in your OS
2. Node and NPM Packages
3. GIT Client

## Steps to start the script

1. Clone the repo and go inside repo
2. run `npm install`
3. run `node index.js`

##### NOTE: Inside the code i have declared some variables to make the process easier, because scraping all the site data takes time. For demo purpose i have limited the category count and articles counts. You can change those values in the code, it will be available below imports.
